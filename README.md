# Nuts And Bolts

Playground project to store and categorize type of nuts and bolts.

Project uses OracleDB from image container, preferably using Podman

## Requirements:

- Gradle 7.6+.
- Spring Boot version 3+.
- Java (OpenJDK) version 19+.
- Podman version 3.4.x+, Compose version 1.0.x+.
- OracleDB (setup your container using [example](./compose.example.yaml) compose file)

>Keywords:
>- Nuts
>- Bolts
>- OracleDB
>- Java
>- Podman

## Author

- [Matija Čvrk](https://hr.linkedin.com/in/matija-%C4%8Dvrk-1388b3101)
