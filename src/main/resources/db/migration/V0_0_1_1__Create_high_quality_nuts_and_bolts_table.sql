DECLARE
  tbl_count number;
  sql_stmt  long;

BEGIN
  SELECT COUNT(*)
  INTO tbl_count
  FROM SYS.USER_ALL_TABLES
  WHERE table_name = 'high_quality';

  IF (tbl_count <= 0)
  THEN
    sql_stmt := '
      CREATE TABLE high_quality (
        id number(6,0),
        grade number,
        strain number,
        ultimate_tensile_strength number,
        ratio number,
        toughness number
      )';
    EXECUTE IMMEDIATE sql_stmt;
  END IF;
END;
