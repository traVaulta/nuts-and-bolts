package cvrk.matija.nab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NutsAndBolts {

    public static void main(String[] args) {
        SpringApplication.run(NutsAndBolts.class, args);
    }

}
